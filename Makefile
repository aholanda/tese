LATEX = latex

tese.pdf: tese.dvi
	dvipdf $<

tese.dvi: tese.tex
	$(LATEX) $<

clean:
	rm -f *.{aux,dvi,lof,ilg,log,lol,lot,out,ps,sigla,siglax,symbols,symbolxs,toc,*~}
