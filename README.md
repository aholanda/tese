# Tese de doutorado

* Autor: [Adriano J. Holanda](http://holanda.xyz)
* Título: ["Proposta de uma Arquitetura Interoperável para um Sistema
  de Informação em Saúde"](http://www.teses.usp.br/teses/disponiveis/59/59135/tde-22052007-102053/pt-br.php)
* Área: [Física Aplicada à Medicina e Biologia](http://prpg.usp.br/famb)
* [Departamento de Física e Matemática](http://dcm.ffclrp.usp.br)
* [Faculdade de Filosofia, Ciências
  e Letras de Ribeirão Preto](http://www.ffclrp.usp.br)
* [Universidade de São Paulo](http://www.usp.br)
* Ano: 2005

Este repositório contem arquivos LaTeX e figuras para geração da tese citada em formato pdf. 
Para gerar automaticamente no `shell`, basta digitar:

````
$ make
````

## Dependência

* LaTeX: [teTeX](https://www.tug.org/tetex/) ou [MiKTeX](http://miktex.org/).
