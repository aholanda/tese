\chapter{The Main Text}
\label{chapter:mainbody1}

Major headings like chapters should be labelled with the 
``$\backslash$chapter'' 
command.  This will automatically put the name of the chapter in the 
table of contents.

\section{A Section}
\label{section:asection}

The next major division after a ``$\backslash$chapter'' 
is a ``$\backslash$section''.  There
are two further divisions called ``$\backslash$subsection'' 
and ``$\backslash$subsubsection''
that may be used.  Using each of these commands will automatically add
a reference in the table of contents.  In this example paper I have 
included a label with each division.  These labels are used to refer to
a section of your document from another section of your document
using the ``$\backslash$ref{}'' command.  For example, the introduction to 
this paper is Chapter ``\ref{chapter:introduction}''.

\subsection{Tables}
\label{section:tables}

Tables are created in this paper by using the environment ``singletable''.
This environment behaves just like the default LATEX table environment
except that it creates a singly spaced table within the doubly spaced main
text.  It can have a caption and a label, but does \em not \em take a location
preference (e.g. [htp]).  This means the table will be placed where LATEX
thinks is best.  This is usually fine, but if it is a problem for you the
definitions used in ``singletable'' (contained in the files 
ExampleThesis.tex and ruthesis.sty) are fairly easy to follow so you can
create your own table with standard commands.  See Table \ref{table:example} 
for an example table.

\begin{singletable}
\begin{tabular}{||r|c|c|c|c||}
\hline\hline
Method  &Maximum Purity $\times\;\epsilon_{s}$
        &$\epsilon_{s}$
        &$\epsilon_{b}$ 
        &$\frac{\epsilon_{s}}{\epsilon_{b}}$\\
\hline
\multicolumn{5}{||l||}{\colorbox[named]{Yellow}{Single electron versus 
      QCD jets}}\\
\hline
\color[named]{PineGreen}{Fixed Kernel PDE}      & .953 & .982 & .0299 & 32.8\\
\color[named]{MidnightBlue}{Adaptive Kernel PDE}& .957 & .982 & .0260 & 37.8\\
\color[named]{Mulberry}{Neural Network}         & .958 & .993 & .0365 & 27.2\\
\hline
\multicolumn{5}{||l||}{\colorbox[named]{Yellow}{Single electron versus 
     $\tau \rightarrow \pi^{0}\rightarrow\gamma\gamma$}}\\
\hline
\color[named]{PineGreen}{Fixed Kernel PDE}      & .844 & .935 & .101  & 9.26 \\
\color[named]{MidnightBlue}{Adaptive Kernel PDE}& .871 & .958 & .0957 & 10.0 \\
\color[named]{Mulberry}{Neural Network}         & .882 & .952 & .0750 & 12.7 \\
\hline\hline
\end{tabular}
\caption{An example of a table in LATEX.  Table structure and 
justification of the entries is done with the arguement of the 
``tabular'' environment.  Entries can be made to extend 
across multiple columns and/or can be highlighted with color.}
\label{table:example}
\end{singletable}

\subsubsection{Colors}
\label{section:colors}

Colored boxes or letters can be used to highlight pieces of text as shown
in Table \ref{table:example}.  
There is a set of 
predefined colors that can be accessed by including the style file 
``color.sty'' (minus the .sty extension) within 
the ``documentstyle'' square brackets.  The colors 
available are:
\begin{singletabbing}
xxxxxxxxxxxxxxxxxx\=xxxxxxxxxxxxxxxxxx\=xxxxxxxxxxxxxxxxxx\=\kill
\colorbox[named]{GreenYellow}{GreenYellow}
\>\colorbox[named]{Yellow}{Yellow}
\>\colorbox[named]{Goldenrod}{Goldenrod}
\>\colorbox[named]{Dandelion}{Dandelion}\\
\colorbox[named]{Apricot}{Apricot}
\>\colorbox[named]{Peach}{Peach}
\>\colorbox[named]{Melon}{Melon}
\>\colorbox[named]{YellowOrange}{YellowOrange}\\
\colorbox[named]{Orange}{Orange}
\>\colorbox[named]{BurntOrange}{BurntOrange}
\>\colorbox[named]{Bittersweet}{\color[named]{White}{BitterSweet}}
\>\colorbox[named]{RedOrange}{RedOrange}\\
\colorbox[named]{Mahogany}{\color[named]{White}{Mahogany}}
\>\colorbox[named]{Maroon}{\color[named]{White}{Maroon}}
\>\colorbox[named]{BrickRed}{\color[named]{White}{BrickRed}}
\>\colorbox[named]{Red}{Red}\\
\colorbox[named]{OrangeRed}{OrangeRed}
\>\colorbox[named]{RubineRed}{RubineRed}
\>\colorbox[named]{WildStrawberry}{WildStrawberry}
\>\colorbox[named]{Salmon}{Salmon}\\
\colorbox[named]{CarnationPink}{CarnationPink}
\>\colorbox[named]{Magenta}{Magenta}
\>\colorbox[named]{VioletRed}{VioletRed}
\>\colorbox[named]{Rhodamine}{Rhodamine}\\
\colorbox[named]{Mulberry}{Mulberry}
\>\colorbox[named]{RedViolet}{\color[named]{White}{RedViolet}}
\>\colorbox[named]{Fuchsia}{\color[named]{White}{Fuchsia}}
\>\colorbox[named]{Lavender}{Lavender}\\
\colorbox[named]{Thistle}{Thistle}
\>\colorbox[named]{Orchid}{Orchid}
\>\colorbox[named]{DarkOrchid}{\color[named]{White}{DarkOrchid}}
\>\colorbox[named]{Purple}{\color[named]{White}{Purple}}\\
\colorbox[named]{Plum}{\color[named]{White}{Plum}}
\>\colorbox[named]{Violet}{\color[named]{White}{Violet}}
\>\colorbox[named]{RoyalPurple}{\color[named]{White}{RoyalPurple}}
\>\colorbox[named]{BlueViolet}{\color[named]{White}{BlueViolet}}\\
\colorbox[named]{Periwinkle}{\color[named]{White}{Periwinkle}}
\>\colorbox[named]{CadetBlue}{\color[named]{White}{CadetBlue}}
\>\colorbox[named]{CornflowerBlue}{CornflowerBlue}
\>\colorbox[named]{MidnightBlue}{\color[named]{White}{MidnightBlue}}\\
\colorbox[named]{NavyBlue}{\color[named]{White}{NavyBlue}}
\>\colorbox[named]{RoyalBlue}{\color[named]{White}{RoyalBlue}}
\>\colorbox[named]{Blue}{\color[named]{White}{Blue}}
\>\colorbox[named]{Cerulean}{Cerulean}\\
\colorbox[named]{Cyan}{Cyan}
\>\colorbox[named]{ProcessBlue}{ProcessBlue}
\>\colorbox[named]{SkyBlue}{SkyBlue}
\>\colorbox[named]{Turquoise}{Turquoise}\\
\colorbox[named]{TealBlue}{TealBlue}
\>\colorbox[named]{Aquamarine}{Aquamarine}
\>\colorbox[named]{BlueGreen}{BlueGreen}
\>\colorbox[named]{Emerald}{Emerald}\\
\colorbox[named]{JungleGreen}{JungleGreen}
\>\colorbox[named]{SeaGreen}{SeaGreen}
\>\colorbox[named]{Green}{Green}
\>\colorbox[named]{ForestGreen}{\color[named]{White}{ForestGreen}}\\
\colorbox[named]{PineGreen}{\color[named]{White}{PineGreen}}
\>\colorbox[named]{LimeGreen}{LimeGreen}
\>\colorbox[named]{YellowGreen}{YellowGreen}
\>\colorbox[named]{SpringGreen}{SpringGreen}\\
\colorbox[named]{OliveGreen}{\color[named]{White}{OliveGreen}}
\>\colorbox[named]{RawSienna}{\color[named]{White}{RawSienna}}
\>\colorbox[named]{Sepia}{\color[named]{White}{Sepia}}
\>\colorbox[named]{Brown}{\color[named]{White}{Brown}}\\
\colorbox[named]{Tan}{\color[named]{White}{Tan}}
\hspace{-1.0cm}\colorbox[named]{Gray}{\color[named]{White}{Gray}}
\>\colorbox[named]{Black}{\color[named]{White}{Black}}
\>White\\
\end{singletabbing}
Be careful when using color that things don't become difficult to read.  
I would suggest coloring the text itself (``$\backslash$color'') in the paper
and using colored backgrounds (``$\backslash$colorbox'') on overhead slides.

\subsection{Figures}
\label{section:figures}

Including a figure in your paper could mean pulling in an external file,
such as a PostScript file, or it could mean telling LATEX what to draw.
LATEX drawing commands are used within the ``picture'' environment.  See
Figure \ref{figure:picture} for an example of this.  Note that the
``picture'' environment does not have to be within the ``figure''
environment.
%%%%%%%%%%%%%%%%%
\begin{figure}[ht]
\begin{center}
%
% The coordinates used by the ``picture'' environment are in 
% millimeters.  I don't have any good ideas for how to use 
% ``picture''.  You'll just have to play around with the 
% numbers until it looks good to you.
%
\begin{picture}(100,100)(0,0)
\put(0,0){\line(1,0){100}}
\put(0,100){\line(1,0){100}}
\put(0,0){\line(0,1){100}}
\put(100,0){\line(0,1){100}}
\put(30,45){\Large JACK}
\end{picture}
\end{center}
\caption{A picture drawn using the LATEX ``picture'' environment.  What
common phrase does this figure stand for?}
\label{figure:picture}
\end{figure}
%-----------

Doing
much more than simple drawings using LATEX can be very tedious though.  I'd
recommend trying the commonly available program ``xfig'' to create drawings
and export them to PostScript or Encapsulated PostScript files that can 
then be pulled into the document.  Figure \ref{figure:fermiring} was
drawn using ``xfig''.
%%%%%%%%%%%%%%%%%
\begin{figure}[ht]
\begin{center}
% The command ``leavevmode'' must be included if you want to center your
% figure using the ``center'' environment within the ``figure'' 
% environment
\leavevmode
\epsffile{fermring2.eps}
\caption{A simplified overhead view of the Fermilab Accelerator drawn
using the X-terminal drawing program ``xfig''.  LATEX is using the image
size defined in the .eps file for this figure.}
\label{figure:fermiring}
\end{center}
\end{figure}
%-----------

LATEX gives you the option of resizing your figure
using the commands ``$\backslash$epsfxsize'' and ``$\backslash$epsfysize''.
The ``x'' is the horizontal axis and the ``y'' is the vertical axis.  You 
can only use one of these commands at a time since LATEX will keep the 
aspect ratio of the image constant.  The only difference that I have 
noticed between PostScript and Encapsulated PostScript 
files in LATEX is in these scaling
commands.  Encapsulated PostScript files can be enlarged or reduced from 
their original size.  Regular PostScript files can \em only be reduced \em.
Figure \ref{figure:calorimeter} is an example of a PostScript file that has 
been reduced to fit on the page.
%%%%%%%%%%%%%%%%%
\begin{figure}[ht]
\begin{center}
\epsfxsize=4in
\leavevmode\epsffile{csi.ps}
\caption{A PostScript image of the KTeV detector calorimeter
(Fermilab Experiments E799 \& E832).  This image has been rescaled using 
the ``$\backslash$epsfxsize'' command.}
\label{figure:calorimeter}
\end{center}
\end{figure}
%-----------

Sometimes your image may be shaped such that it would fit on the
page better if it were sideways.  Doing so is not easy, but it can be
done using the ``picture'' environment and the command 
``$\backslash$rotatebox''.  You will need to play with the 
numerical parameters that are the arguements for the 
``$\backslash$put'' to get your own image to look reasonable.  Figure
\ref{figure:PDE2dres} is an example of a rotated image and caption.
%%%%%%%%%%%%%%%%%
\begin{figure}[p]
\begin{picture}(410,575)
\put(0,4){\rotatebox{90}{
      \epsfbox{pde2Dres1.eps}}}
\put(393,80){\rotatebox{90}{
      \vbox{\caption{An example of a figure that needs to be turned 
sideways on the page to increase its size and improve its legibility.}}}}
\end{picture}
\label{figure:PDE2dres}
\end{figure}
%-----------

It is easiest if the PostScript files to be included are placed in the 
same directory as the .tex files, but they can be anywhere provided the
entire pathway name is given as the arguement to the $\backslash$epsffile
command.

\subsection{Equations}
\label{section:equations}

LATEX is very good at displaying equations, including ones with non-standard
alpha-numeric characters.  In the base file for this paper 
(``ExampleThesis.tex''), there are two new commands defined for displaying
equations that are both shorter than the basic LATEX commands, and that handle
the switch between double- and single-line spacing.  To begin an equation
expression use ``$\backslash$be'', and to end it use ``$\backslash$ee''.
For example, see the file ``mainbody1.tex'' for the code to display these
equations:
  \be\mid K_{S}\rangle\; =\;\frac{1}{\sqrt{(1 + \mid \epsilon \mid ^{2})}}  
    [ |K_{1}\rangle + \epsilon|K_{2}\rangle] \ee
  \be\mid K_{L}\rangle\; =\;\frac{1}{\sqrt{(1 + \mid \epsilon \mid ^{2})}}  
    [ |K_{2}\rangle + \epsilon|K_{1}\rangle] \ee
A slightly more complex equation example is:
  \be\epsilon = \frac{\langle K^{0}\mid{\bf H}\mid\bar{K}^{0}\rangle - 
    \langle\bar{K}^{0}\mid {\bf H} \mid K^{0}\rangle}
    {i({\mathit \Gamma_{S}} - {\mathit \Gamma_{L}})/2 - (m_{S} - m_{L})}\ee

\subsection{Citations}
\label{section:citations}

Citations in LATEX are extremly simple.  The number of the reference shows 
up in the text within square brackets but is refered to in the code by a
name that you define using the command ``$\backslash$cite''.  
It works just like the ``$\backslash$ref'' command.
For example, if you are interested in high energy
physics, then a possible starting point is a book written by Donald H.
Perkins \cite{Perkins}.  The rest of the entries in the bibliography of this
paper are just various examples of formatting possiblities.
